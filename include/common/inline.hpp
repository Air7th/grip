#pragma once


#include <common/compiler.hpp>
#include <common/platform.hpp>


#ifndef GRIP_INLINE
    #if GRIP_PLATFORM_WIN && GRIP_COMPILER_MSVC
        #define GRIP_INLINE inline
    #endif
#endif
#ifndef GRIP_INLINE
    #define GRIP_INLINE
#endif


#ifndef GRIP_FORCEINLINE
    #if GRIP_PLATFORM_WIN && GRIP_COMPILER_MSVC
        #define GRIP_FORCEINLINE __forceinline
    #endif
#endif
#ifndef GRIP_FORCEINLINE
    #define GRIP_FORCEINLINE
#endif
