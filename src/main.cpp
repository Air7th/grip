

#include <tchar.h>
#include <Windows.h>
#include <string>
#include <sstream>
#include <grip.hpp>


#define UNUSED(x)


int APIENTRY _tWinMain(
	HINSTANCE UNUSED(hInstance),
	HINSTANCE UNUSED(hPrevInstance),
	LPTSTR    UNUSED(lpCmdLine),
	int       UNUSED(nCmdShow)
)
{
    using namespace std;
    stringstream ss;

    ss << "Hello! Visual Studio 2017!\n" << grip::function() << endl;

    OutputDebugStringA(ss.str().c_str());

	return 0;
}

